#ifndef CALLBACKS_INCLUDED
#define CALLBACKS_INCLUDED
#include <GLFW/glfw3.h>

void printHello(GLFWwindow* window, int key, int scancode, int action, int mods);

#endif