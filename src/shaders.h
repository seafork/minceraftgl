//
// Created by seafork on 8/17/22.
//

#ifndef MINCERAFT_SHADERS_H
#define MINCERAFT_SHADERS_H
#include <glad/glad.h>
#include <stdio.h>
#include <stdlib.h>

GLuint makeShaderProgram(const char* vertexSource, const char* fragmentSource);
void deleteShaderProgram(GLuint ShaderProgram);
#endif //MINCERAFT_SHADERS_H
