//
// Created by seafork on 8/17/22.
//

#include "EBO.h"
GLuint EBO(GLfloat* indices, GLsizeiptr size)
{
    GLuint EBO = 0;
    glGenBuffers(1, &EBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, indices, GL_STATIC_DRAW);
    return EBO;
}
void bindEBO(GLuint VBO)
{
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
}
void unbindEBO()
{
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}
void deleteEBO(GLuint* EBO)
{
    glDeleteBuffers(1, EBO);
}
