#include <stdio.h>
#include <stdlib.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <cglm/cglm.h>
#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>
#include "callbacks.h"
#include "shaders.h"
#include "VBO.h"
#include "EBO.h"
#include "VAO.h"

int main(void) {

    // Settings
    int canResise = 0;
    int startFullscreen = 0;

    if (!glfwInit())
        return -1;

    // Window hints
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

    if (canResise)
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);
    
    if (startFullscreen)
        glfwWindowHint(GLFW_MAXIMIZED, GLFW_TRUE);

    GLFWwindow* window = glfwCreateWindow(
        800, 
        600, 
        "Hello World", 
        NULL, 
        NULL
        
    );

    if (!window)
    {
        glfwTerminate();
        return -1;
    }

    glfwMakeContextCurrent(window);

    glfwSetKeyCallback(window, printHello);


    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
        return -1;

// Vertices coordinates
    GLfloat square_vertices[] =
            { //     COORDINATES     /        COLORS      /   TexCoord  //
                    -0.5f, -0.5f, 0.0f,     1.0f, 0.0f, 0.0f,	0.0f, 0.0f, // Lower left corner
                    -0.5f,  0.5f, 0.0f,     0.0f, 1.0f, 0.0f,	0.0f, 1.0f, // Upper left corner
                    0.5f,  0.5f, 0.0f,     0.0f, 0.0f, 1.0f,	1.0f, 1.0f, // Upper right corner
                    0.5f, -0.5f, 0.0f,     1.0f, 1.0f, 1.0f,	1.0f, 0.0f  // Lower right corner
            };

// Indices for vertices order
    GLuint indices[] =
            {
                    0, 2, 1, // Upper triangle
                    0, 3, 2 // Lower triangle
            };

    // Create vertex array object
    GLuint vertexArrayObject = VAO();

    // Bind the vertex array object as current
    // Making and binding the VAO first allows it to track the changes
    // made to the element array object and our vertex buffer object.
    bindVAO(vertexArrayObject);

    // Create vertex buffer array
    GLuint vertexBufferObject = VBO(square_vertices,
                                    sizeof(square_vertices));
    // Create element buffer array
    GLuint elementBufferObject = EBO(indices, sizeof(indices));

    linkAttrib(vertexBufferObject, 0, 3, GL_FLOAT,
               8 * sizeof(float), (void*)0);
    linkAttrib(vertexBufferObject, 1, 3, GL_FLOAT,
               8 * sizeof(float), (void*)(3 * sizeof(float)));
    linkAttrib(vertexBufferObject, 2, 2, GL_FLOAT,
               8 * sizeof(float), (void*)(6 * sizeof(float)));

    // Unbind all buffers
    unbindVAO();
    unbindVBO();
    unbindEBO();

    // Generate shaders
    GLuint shaderProgram = makeShaderProgram
            ("/home/seafork/Documents/Projects/MinceRaft/shader/default.vert","/home/seafork/Documents/Projects/MinceRaft/shader/default.frag");

    // For texture
    int imageWidth, imageHeight, numColorChannels;
    stbi_set_flip_vertically_on_load(true);
    unsigned char* imageBytes = stbi_load("/home/seafork/Documents/Projects/MinceRaft/pictures/image.png",
                                          &imageWidth, &imageHeight, &numColorChannels, 0);
    GLuint texture;
    glGenTextures(1, &texture);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                    GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
                    GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,
                    GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,
                    GL_REPEAT);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, imageWidth,
                 imageHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE,
                 imageBytes);
    glGenerateMipmap(GL_TEXTURE_2D);

    // Clear data
    stbi_image_free(imageBytes);
    glBindTexture(GL_TEXTURE_2D, 0);

    // Uniform
    GLuint tex0Uni = glGetUniformLocation(shaderProgram, "tex0");
    // Use shader program
    glUseProgram(shaderProgram);
    glUniform1f(tex0Uni, 0);

    // Gl Clear functions
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    // Swap first buffer
    glfwSwapBuffers(window);

    while (!glfwWindowShouldClose(window))
    {
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        glBindTexture(GL_TEXTURE_2D, texture);

        // Use this vertex array which stores our vertex data
        bindVAO(vertexArrayObject);
        // Draw vertices as triangles
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

        glfwPollEvents();
        glfwSwapBuffers(window);
    }

    // Clean up
    deleteShaderProgram(shaderProgram);
    deleteVAO(&vertexArrayObject);
    deleteVBO(&vertexBufferObject);
    deleteEBO(&elementBufferObject);
    glfwDestroyWindow(window);
    return 0;
}

