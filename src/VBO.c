//
// Created by seafork on 8/17/22.
//

#include "VBO.h"

GLuint VBO(GLfloat* vertices, GLsizeiptr size)
{
    // Configure the vertex attributes so that OpenGl knows how to read
    // the VBO
    /* 0 is the attribute number which we set earlier.
     * 3 means the type of vector we want
     * We then tell GL the data is a float
     * We then tell it to be normalized
     * We then tell it the stride, which in our case is 3 * sizeof(float)
     * Then the pointer is where our data starts in the array */
    GLuint VBO = 0;
    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, size, vertices, GL_STATIC_DRAW);
    return VBO;
}
void bindVBO(GLuint VBO)
{
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
}
void unbindVBO()
{
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}
void deleteVBO(GLuint* VBO)
{
    glDeleteBuffers(1, VBO);
}
