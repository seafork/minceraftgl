//
// Created by seafork on 8/17/22.
//
#include "shaders.h"

char* loadFile(const char* filepath)
{
    FILE *pFile;
    long lSize;
    char* buffer;

    pFile = fopen(filepath, "rb");
    if(!pFile)
        perror(filepath), exit(1);

    fseek(pFile, 0L, SEEK_END);
    lSize = ftell(pFile);
    rewind(pFile);

    buffer = calloc(1, lSize+1);
    if(!buffer)
    {
        fclose(pFile);
        fputs("memory allocation failed", stderr);
        exit(1);
    }

    if (1 != fread(buffer, lSize, 1, pFile))
    {
        fclose(pFile);
        free(buffer);
        fputs("entire read fails",stderr);
        exit(1);
    }

    fclose(pFile);
    return buffer;
};


GLuint makeShaderProgram(const char* vertexSource, const char* fragmentSource)
{
    const char* vertexShaderSource = loadFile(vertexSource);
    const char* fragmentShaderSource = loadFile(fragmentSource);

    // Create vertex shader
    GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
    // Bind shader source to shader
    glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
    // Compile shader
    glCompileShader(vertexShader);

    // Create fragment shader
    GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    // Bind shader source to shader
    glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
    // Compile shader
    glCompileShader(fragmentShader);

    // Create shader program
    GLuint shaderProgram = glCreateProgram();
    // Attach shaders
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);
    // Link program together
    glLinkProgram(shaderProgram);

    // Clean up shaders
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    return shaderProgram;
};

void deleteShaderProgram(GLuint ShaderProgram)
{
    glDeleteProgram(ShaderProgram);
}