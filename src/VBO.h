//
// Created by seafork on 8/17/22.
//

#ifndef MINCERAFT_VBO_H
#define MINCERAFT_VBO_H
#include <glad/glad.h>
GLuint VBO(GLfloat* vertices, GLsizeiptr size);
void bindVBO(GLuint VBO);
void unbindVBO();
void deleteVBO(GLuint* VBO);
#endif //MINCERAFT_VBO_H
