//
// Created by seafork on 8/17/22.
//

#include "VAO.h"
GLuint VAO()
{
    GLuint VAO = 0;
    glGenVertexArrays(1, &VAO);
    return VAO;
}
void linkAttrib(GLuint VBO, GLuint layout, GLuint numComponents, GLenum type, GLsizeiptr stride, void* offset)
{
    bindVBO(VBO);
    glVertexAttribPointer(layout, numComponents, type, GL_FALSE, stride, (void*)offset);
    glEnableVertexAttribArray(layout);
    unbindVBO();

}
void bindVAO(GLuint VAO)
{
    glBindVertexArray(VAO);
}
void unbindVAO()
{
    glBindVertexArray(0);
}
void deleteVAO(GLuint* VAO)
{
    glDeleteVertexArrays(1, VAO);
}