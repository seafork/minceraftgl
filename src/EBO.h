//
// Created by seafork on 8/17/22.
//

#ifndef MINCERAFT_EBO_H
#define MINCERAFT_EBO_H
#include <glad/glad.h>
GLuint EBO(GLfloat* indices, GLsizeiptr size);
void bindEBO(GLuint EBO);
void unbindEBO();
void deleteEBO(GLuint* EBO);
#endif //MINCERAFT_EBO_H
