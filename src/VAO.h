//
// Created by seafork on 8/17/22.
//

#ifndef MINCERAFT_VAO_H
#define MINCERAFT_VAO_H
#include <glad/glad.h>
#include "VBO.h"
GLuint VAO();
void linkAttrib(GLuint VBO, GLuint layout, GLuint numComponents, GLenum type, GLsizeiptr stride, void* offset);
void bindVAO(GLuint VAO);
void unbindVAO();
void deleteVAO(GLuint* VAO);
#endif //MINCERAFT_VAO_H
